package common

// GitTagVersion is a version string taken from the latest git tag
var GitTagVersion string

// GetVersion returns app version string
func GetVersion() string {
	if GitTagVersion == "" {
		return "master"
	}

	return GitTagVersion
}
