# Go build
FROM golang:1.17-alpine AS gobuild

LABEL description="Config files converter CLI tool" vendor="kukymbr"

RUN mkdir -p /go/src/kukymbr/confconvert
WORKDIR /go/src/kukymbr/confconvert
COPY . .

RUN apk add --update --no-cache git gcc g++ make
RUN make build

# Runner
FROM alpine:3.8

RUN mkdir /app
WORKDIR /app
COPY --from=gobuild /go/src/kukymbr/confconvert/confconvert ./

ENTRYPOINT ["/app/confconvert"]
