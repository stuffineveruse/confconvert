package convert

import (
	"confconvert/converter"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetConverter(t *testing.T) {
	{
		t.Log("Testing invalid converters")

		names := []string{
			"wookiee",
			"chewbacca",
		}

		for _, name := range names {
			conv, err := getConverter(name)
			assert.Nil(t, conv, name)
			assert.Error(t, err, name)
		}
	}

	{
		t.Log("Testing valid converters")

		conv, err := getConverter(ConvNameViper)
		assert.IsType(t, &converter.ViperConv{}, conv)
		assert.NoError(t, err)
	}
}
