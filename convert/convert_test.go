package convert

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOpt_Validate(t *testing.T) {
	{
		t.Log("Testing invalid options")

		options := []*Opt{
			{},
			{From: "file.json"},
			{To: "file.json"},
			{From: "unknownfile.json", To: "config.yaml"},
			{From: "unknownfile.yaml", To: "config.json"},
		}

		for i, opt := range options {
			err := opt.Validate()
			assert.Error(t, err, i)
		}
	}

	{
		t.Log("Testing valid options")

		options := []*Opt{
			{From: "../example/config.json", To: "config.yaml"},
			{From: "../example/config.yaml", To: "config.json"},
		}

		for i, opt := range options {
			err := opt.Validate()
			assert.NoError(t, err, i)
		}
	}
}
