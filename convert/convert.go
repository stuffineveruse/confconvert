package convert

import (
	"errors"
)

// Converters names
const (
	ConvNameViper   = "viper"
	ConvNameDefault = ConvNameViper
)

// Opt is a Convert tool options
type Opt struct {
	From      string
	To        string
	Converter string
}

// Validate validates input values
func (o *Opt) Validate() (err error) {
	if o.From == "" {
		return errors.New("no `from` option given")
	}

	if o.To == "" {
		return errors.New("no `to` option given")
	}

	if err = checkInpPath(o.From); err != nil {
		return err
	}

	return nil
}

// NewConvert creates new Convert tool instance
func NewConvert(opt *Opt) *Convert {
	return &Convert{opt: opt}
}

// Convert is a tool to convert config files
type Convert struct {
	opt *Opt
}

// Run executes the converter
func (c *Convert) Run() (err error) {
	conv, err := getConverter(c.opt.Converter)
	if err != nil {
		return err
	}

	return conv.Convert(c.opt.From, c.opt.To)
}
