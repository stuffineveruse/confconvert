package convert

import (
	"confconvert/converter"
	"errors"
	"os"
)

// checkInpPath checks if input file path is OK
func checkInpPath(path string) error {
	_, err := os.Stat(path)
	if err == nil {
		return nil
	}

	return errors.New("cannot to read file " + path + ": " + err.Error())
}

// getConverter initializes the Converter from the conv name
func getConverter(convName string) (conv converter.Converter, err error) {
	switch convName {
	case ConvNameViper:
		return &converter.ViperConv{}, nil
	}

	return nil, errors.New("unknown converter: " + convName)
}
