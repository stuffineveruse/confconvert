package converter

import "github.com/spf13/viper"

// ViperConv converts files via the viper
type ViperConv struct {
}

// Convert converts config file from one format to another
func (c *ViperConv) Convert(from string, to string) (err error) {
	cfg := viper.New()
	cfg.SetConfigFile(from)

	err = cfg.ReadInConfig()
	if err != nil {
		return err
	}

	err = cfg.SafeWriteConfigAs(to)
	if err != nil {
		return err
	}

	return nil
}
