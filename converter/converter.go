package converter

// Converter interface
type Converter interface {

	// Convert converts config file from one format to another
	Convert(from string, to string) error
}
