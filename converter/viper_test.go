package converter

import (
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestViperConv_Convert(t *testing.T) {
	paths := map[string]string{
		"../example/config.json": "../example/config.out.yaml",
		"../example/config.yaml": "../example/config.out.json",
	}

	keys := []string{
		"app.title",
		"app.description",
		"todo",
	}

	conv := &ViperConv{}

	for inp, out := range paths {
		err := conv.Convert(inp, out)
		if !assert.NoError(t, err, inp) {
			continue
		}

		vprIn := viper.New()
		vprIn.SetConfigFile(inp)
		err = vprIn.ReadInConfig()
		if !assert.NoError(t, err, inp) {
			continue
		}

		vprOut := viper.New()
		vprOut.SetConfigFile(out)
		err = vprOut.ReadInConfig()
		if !assert.NoError(t, err, inp) {
			continue
		}

		for _, key := range keys {
			assert.EqualValues(t, vprIn.Get(key), vprOut.Get(key), inp, key)
		}

		err = os.Remove(out)
		assert.NoError(t, err, inp)
	}
}
