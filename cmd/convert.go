package cmd

import (
	"confconvert/convert"
	"github.com/spf13/cobra"
)

var convertOpt = &convert.Opt{}

var convertCmd = &cobra.Command{
	Use:     "convert",
	Short:   "Convert configs files",
	Long:    `Converts configs files from/to YAML, JSON and etc.`,
	Example: `confconvert convert --from=config.json --to=config.yml --conv=viper`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		if err = convertOpt.Validate(); err != nil {
			return err
		}

		tool := convert.NewConvert(convertOpt)

		return tool.Run()
	},
}

func init() {
	rootCmd.AddCommand(convertCmd)

	convertCmd.Flags().StringVar(&convertOpt.From, "from", "", "File to convert from")
	convertCmd.Flags().StringVar(&convertOpt.To, "to", "", "File to convert to")
	convertCmd.Flags().StringVar(&convertOpt.Converter, "conv", convert.ConvNameDefault, "Converter to use")
}
