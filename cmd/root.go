package cmd

import (
	"confconvert/common"
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "confconvert",
	Short: "Convert configs files",
	Long:  `Converts configs files from/to YAML, JSON and etc.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.Version = common.GetVersion()
	cobra.CheckErr(rootCmd.Execute())
}
