## confconvert

`confconvert` is a CLI tool to covert configs files.

### Usage

```
confconvert convert [flags]

Examples:
confconvert convert --from=config.json --to=config.yml --conv=viper

Flags:
      --conv string   Converter to use (default "viper")
      --from string   File to convert from
  -h, --help          help for convert
      --to string     File to convert to
```