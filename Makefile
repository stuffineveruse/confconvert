all: build

build:
	go mod tidy
	go vet ./...
	go test -race -coverprofile=coverage.out ./...
	go tool cover -func=coverage.out
	go tool cover -html=coverage.out -o coverage.html
	rm -f coverage.out
	go install golang.org/x/lint/golint@latest
	golint -set_exit_status=true ./...
	go build -tags=jsoniter -ldflags "-X main.GitTagVersion=`git tag --sort=-version:refname | head -n 1`"

build_fast:
	go mod tidy
	go test -failfast -short -coverprofile=coverage.out ./...
	go tool cover -func=coverage.out
	go tool cover -html=coverage.out -o coverage.html
	rm -f coverage.out
	go build -tags=jsoniter -ldflags "-X main.GitTagVersion=`git tag --sort=-version:refname | head -n 1`"

clean:
	go clean