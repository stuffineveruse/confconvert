package main

import (
	"confconvert/cmd"
	"confconvert/common"
)

// GitTagVersion is a version string taken from the latest git tag
var GitTagVersion string

// confconvert is a CLI too to convert configs files
// from/to the YAML, JSON and other formats.
func main() {
	common.GitTagVersion = GitTagVersion
	cmd.Execute()
}
